import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';

import { Movement } from '../model/movement';
import { MovementService } from '../services/movement.service';
import { NotifierService } from '../services/notifier.service';
import { MatSnackBar } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { Message } from "../model/message";
import { Account } from '../model/account';

import * as jwtDecode from 'jwt-decode';

@Component({
  selector: 'movements-app',
  templateUrl: './movements.html',
  styleUrls: ['./movements.css'],
})

export class MovementsComponent implements OnInit {
  filterFormGroup: FormGroup;
  dni: string;
  channel: string;
  account: string;
  user: string;
  messages: String[];
  public dataSource;
  accounts: Account[];

  displayedColumns = ['Id', 'Fecha', 'Hora', 'Monto', 'Moneda'];

  constructor(private service: MovementService, private notifierService: NotifierService, private fb: FormBuilder,
    public snackBar: MatSnackBar, private router: Router) {
  }

  ngOnInit() {
    this.messages = [];
    const token = localStorage.getItem('token');
    const tokenPayload = jwtDecode(token);
    this.dni = tokenPayload.dni;
    this.channel = tokenPayload.channel;
    this.user = tokenPayload.account;

    this.filterFormGroup = this.fb.group({
      'phone': ['', Validators.required],
      'account': [this.account],
      'date': [new Date(), Validators.required],
      'dni': [this.dni],
      'channel': [this.channel],
    });

    this.buildAccounts();
  }

  buildAccounts(): void {
    this.accounts = [
      { id: "000000-00000000-C", description: 'Ripley Clásica' },
      { id: "000000-00000000-S", description: 'Ripley Silver Mastercard' }
    ];
  }

  filterMovements(): void {
    let valid = this.filterFormGroup.valid;
    if (valid) {
      this.service.getMovements(this.filterFormGroup.value.phone, this.filterFormGroup.value.account.id, ((this.filterFormGroup.value.date).toISOString()))
        .subscribe(response  => {
          this.dataSource = response.body;
          localStorage.setItem("activity",response.headers.get('X-Custom-Header'))
          this.sendSms(this.filterFormGroup.value.account.id, this.dataSource);
        },
          (err: HttpErrorResponse) => {
            if (401 == err.status) {
              this.validateExpiration();
            }
            this.openSnackBar(err.error, "X");
            this.dataSource = null;
          });

    } else {
      this.openSnackBar("Ingrese los datos solicitados", "X")
    }
  }

  private validateExpiration() {
    localStorage.clear();
    this.router.navigate(['/login']);

  }

  sendSms(account: string, data: Movement[]): void {
    let smsMessage: Message;
    smsMessage = new Message();
    smsMessage.to = this.filterFormGroup.value.phone;
    smsMessage.text = "Cuenta: " + account + "<br/><br/>Tus últimos movimientos<br/><br/>Fecha y Hora&emsp;&emsp;&emsp;Importe<br/>------------------------------------";

    data.forEach(item => {
      smsMessage.text += "<br/>" + item.date + " " + item.hour + "  &emsp;" + item.currency + " " + item.amount;
    });

    this.notifierService.sms(smsMessage)
      .subscribe(response  => {
          localStorage.setItem("activity",response.headers.get('X-Custom-Header'))

          this.messages.push("Se envió el SMS al número (" + smsMessage.to + "):<br/>*" + smsMessage.text + "*");
        },
        (err: HttpErrorResponse) => {
          this.openSnackBar(err.error, "X");
        }
      );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
