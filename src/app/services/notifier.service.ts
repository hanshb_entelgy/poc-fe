import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';

import { Message } from '../model/message';

@Injectable()
export class NotifierService {
  private serviceUrlSMS = environment.apiUrl.concat('/notifier/sms');
  private serviceUrlEmail = environment.apiUrl.concat('/notifier/email');

  constructor(private http: HttpClient) { }

  sms(message: Message): Observable<HttpResponse<any>> {
    return this.http.post<any>(this.serviceUrlSMS, message, {
      observe: 'response',
      headers: {
        "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
      }
    });
  }

  email(message: Message): Observable<any> {
    return this.http.post<any>(this.serviceUrlEmail, message, {
      observe: 'response',
      headers: {
        "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
      }
    });
  }
}
