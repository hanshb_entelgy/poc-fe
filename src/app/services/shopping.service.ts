import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { catchError, map, tap } from 'rxjs/operators';

import 'rxjs/add/operator/map';

import { Shopping, Oc } from '../model/shopping';

@Injectable()
export class ProductoService {
  private serviceUrlStock = environment.apiUrl.concat('/products/');

  constructor(private http: HttpClient, private router: Router) { }

  getVerifyStockProducto(productId: string, quantity: string): Observable<HttpResponse<Shopping>> {
    return this.http.get<Shopping>(this.serviceUrlStock.concat(productId, "/stock"), {
      observe: 'response',
      params: { 'request.quantity': quantity },
      headers: {
        "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
      }
    });
  }

  postReserveStockProducto(shopping: Shopping): Observable<HttpResponse<Shopping>> {
    return this.http.post<Shopping>(this.serviceUrlStock.concat(shopping.product.id, "/stock"), shopping, {
      observe: 'response',
      params: { 'request.quantity': shopping.requestQuantity.toString(), 'email': shopping.email },
      headers: {
        "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
      }
    });
  }



}