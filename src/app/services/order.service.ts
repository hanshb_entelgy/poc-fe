import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { catchError, map, tap } from 'rxjs/operators';

import 'rxjs/add/operator/map';

import { Shopping, Oc } from '../model/shopping';

@Injectable()
export class OrderService {
    private serviceUrlOc = environment.apiUrl.concat('/oc');

    private serviceUrlOtp = environment.apiUrl.concat('/otp');
    constructor(private http: HttpClient, private router: Router) { }

    generateOC(shopping: Shopping, otp: string, addres: string): Observable<HttpResponse<Shopping>> {
        return this.http.post<Shopping>(this.serviceUrlOc, shopping, {
            observe: 'response',
            params: {
                'otp': otp,
                'address': addres
            },
            headers: {
                "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
            }
        });
    }

    validateOTP(shopping: Shopping, otp: string): Observable<HttpResponse<Oc>> {
        return this.http.post<Oc>(this.serviceUrlOtp, shopping, {
            observe: 'response',
            params: { 'otp': otp },
            headers: {
                "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
            }
        });
    }

}
