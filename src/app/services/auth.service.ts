import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


import { environment } from '../../environments/environment';

import { User } from '../model/user';


@Injectable()
export class AuthService {
    private serviceUrl = environment.apiUrl.concat('/auth');

    constructor(private http: HttpClient) { }

    emailLogin(dni: string, password: string, apiKey: string, account: string, channel:string): Observable<any> {
        return this.http.post<any>(this.serviceUrl, {}, {
            params: { 'dni': dni, 'password': password, 'account': account, 'channel':channel },
            headers: { 'api-key': apiKey, 'poc-app': 'PoC Banco Ripley' }
        });
    }
}