import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { catchError, map, tap } from 'rxjs/operators';

import 'rxjs/add/operator/map';

import { Shopping, Oc } from '../model/shopping';

@Injectable()
export class StockService {
  private stockUrl = environment.apiUrl.concat('/stock');
  constructor(private http: HttpClient, private router: Router) { }

  locateStock(shopping: Shopping): Observable<HttpResponse<Shopping>> {
    return this.http.get<Shopping>(
      this.stockUrl, {
        observe: 'response',
        params: { 'product.id': shopping.product.id, 'local.id': shopping.local.id },
        headers: {
          "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
        }
      });
  }

  decreaseStock(shopping: Shopping): Observable<HttpResponse<Shopping>> {
    return this.http.post<Shopping>(this.stockUrl, shopping, {
      observe: 'response',
      params: {
        'product.id': shopping.product.id,
        'local.id': shopping.local.id,
        'request.quantity': shopping.requestQuantity.toString()
      },
      headers: {
        "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
      }
    });
  }



}
