import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';;

import { Movement } from '../model/movement';

@Injectable()
export class MovementService {
  private serviceUrl = environment.apiUrl.concat('/movements');
  constructor(private http: HttpClient) { }

  getMovements(phone: string, account: string, date: string): Observable<HttpResponse<Movement[]>> {
    return this.http.get<Movement[]>(this.serviceUrl, {
      observe: 'response',
      params: { phone: phone, account: account, date: date },
      headers: {
        "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
      }
    });
  }

}
