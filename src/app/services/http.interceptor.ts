import {    HttpInterceptor, HttpRequest, HttpHandler,HttpEvent,HttpResponse} from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';


export class Interceptor implements HttpInterceptor{

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const started = Date.now();
        debugger;
        return next
        .handle(req)
        .do(event => {
          if (event instanceof HttpResponse) { //<-- HERE
            const elapsed = Date.now() - started;
            console.log(event);
          }
        });

    }
}