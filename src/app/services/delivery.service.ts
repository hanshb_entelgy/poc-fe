import { Injectable } from '@angular/core';
import { HttpClient,HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { catchError, map, tap } from 'rxjs/operators';

import 'rxjs/add/operator/map';

import { Shopping, Oc } from '../model/shopping';

@Injectable()
export class DeliveryService {
    private deliveryUrl = environment.apiUrl.concat('/delivery');
    constructor(private http: HttpClient, private router: Router) { }

    deliveryAddress(shopping: Shopping, address: string): Observable<HttpResponse<Oc>> {
        return this.http.post<Oc>(this.deliveryUrl, shopping, {observe: 'response',
            params: { 'address': address },
            headers: {
                "tsec": localStorage.getItem("token"), "activity": localStorage.getItem("activity")
            }
        });
    }

}