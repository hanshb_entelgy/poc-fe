import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { RouterModule, Routes } from '@angular/router';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

//component

import { ToolbarNav } from './toolbar-menu/toolbar-nav.componet';
import { MovementsComponent } from './movements-component/movements.component';
import { ShoppingComponent } from './shopping/shopping.component';
import { LoginComponent } from './login/login.component';
//service

import { MovementService } from './services/movement.service';
import { ProductoService } from './services/shopping.service';
import { AuthService } from './services/auth.service';
import { EnsureAuthenticated } from './services/ensure.auth.service';
import { StockService } from './services/stock.service';
import { NotifierService } from './services/notifier.service';
import { OrderService } from './services/order.service';
import { DeliveryService } from './services/delivery.service';


import { Route } from '@angular/compiler/src/core';
import { Movement } from './model/movement';
import { Shopping } from './model/shopping';
import { Interceptor } from './services/http.interceptor';



const appRoutes: Routes = [

  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'ultimo.movimiento', component: MovementsComponent, canActivate: [EnsureAuthenticated] },
  { path: 'orden.compra', component: ShoppingComponent, canActivate: [EnsureAuthenticated] },
];

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    ToolbarNav,
    MovementsComponent,
    ShoppingComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [NotifierService,
    MovementService,
    ProductoService,
    AuthService,
    EnsureAuthenticated,
    StockService,
    OrderService,
    NotifierService,
    DeliveryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
