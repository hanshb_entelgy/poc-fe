export class Shopping {
    reservationCode: string;
    product: Product;
    local: Local;
    code: string;
    account: string;
    requestQuantity: number;
    email: string;
    amount: number;
    cantComprada:number;
    cantDisponible:number;
    oc:string;
}

export class Oc{
    ordenCompra:Shopping;
    address:string;
}


export class Product {
    id: string;
    name: string;
    currency: string;
    amount: Number;
}

export class Local {
    id: string;
    name: string;
    address: string;
}
