export class Movement{
  id:number;
  date:string;
  hour:string;
  amount:string;
  currency:string;
}