import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpHeaderResponse } from '@angular/common/http';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material';

import { AuthService } from '../services/auth.service';
import { User } from '../model/user';
import { Canal } from '../model/canal';

@Component({
    selector: 'login-app',
    templateUrl: './login.html',
    styleUrls: ['./login.css'],
})

export class LoginComponent implements OnInit {
    userForm: FormGroup;
    formErrors = {
        'dni': '',
        'password': '',
        'account':'',
        'channel':''
    };
    canals: Canal[];
    constructor(private router: Router,
        private fb: FormBuilder,
        private auth: AuthService,
        public snackBar: MatSnackBar) { }
    ngOnInit() {
        this.getCanal();
        this.userForm = this.fb.group({
            'dni': ['', [
                Validators.required
            ]],
            'account': ['', [
                Validators.required
            ]],
            'password': ['', [
                Validators.required
            ]],
            'channel': ['', [Validators.required
            ]],
        });
    }

    getCanal(): void {
        this.canals = [
            { description: 'Canal Movimientos', key: this.guidGenerator() },
            { description: 'Canal Compra', key: this.guidGenerator() },
            { description: 'Canal Sin Acceso', key: this.guidGenerator() },
            { description: 'Canal Movimientos y Compra 1', key: this.guidGenerator() },
            { description: 'Canal Movimientos y Compra 2', key: this.guidGenerator() }
        ];
    }

    guidGenerator() {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }

    login(): void {
        let valid = this.userForm.valid;
        if (valid) {
            this.auth.emailLogin(this.userForm.value['dni'],
                this.userForm.value['password'],
                this.userForm.value['channel'].key,
                this.userForm.value['account'],
                this.userForm.value['channel'].description)
                .subscribe(response => {
                    localStorage.setItem('token', response.tsec);
                    localStorage.setItem('activity', response.experidTime.toString());
                    this.afterSignIn()
                },
                    (err: HttpErrorResponse) => {
                        if (0 == err.status) {
                            this.openSnackBar("Se produjo un error vuelve a intentarlo más tarde", "ok")
                        }
                        this.openSnackBar(err.error, "X")
                    });

        } else {
            this.openSnackBar("Completar los campos requeridos", "X")        }
    }

    private afterSignIn() {
        this.router.navigate(['/ultimo.movimiento']);
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 2000,
        });
    }



}
