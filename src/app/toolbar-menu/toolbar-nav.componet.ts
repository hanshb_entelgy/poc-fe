import { Component, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

import { Router } from '@angular/router';

@Component({
    selector: 'toolbar-nav',
    templateUrl: './toolbar-nav.html',
    styleUrls: ['./toolbar-nav.css']
})

export class ToolbarNav {
    mobileQuery: MediaQueryList;
    private _mobileQueryListener: () => void;
    constructor(private changeDetectorRef: ChangeDetectorRef, private media: MediaMatcher,private router: Router,) {
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }

    logout(): void {
        localStorage.clear();
        this.router.navigate(["/login"]);
    }
}