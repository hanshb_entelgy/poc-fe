import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import "rxjs/add/observable/of";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";

import { ProductoService } from "../services/shopping.service";
import { StockService } from "../services/stock.service";
import { OrderService } from "../services/order.service";
import { NotifierService } from "../services/notifier.service";

import { MatSnackBar } from "@angular/material";

import * as jwtDecode from "jwt-decode";

import { Product } from "../model/product";
import { Account } from "../model/account";
import { Oc, Shopping } from "../model/shopping";
import { Message } from "../model/message";
import { DeliveryService } from "../services/delivery.service";

@Component({
  selector: 'shopping-app',
  templateUrl: './shopping.html',
  styleUrls: ['./shopping.css'],
})

export class ShoppingComponent implements OnInit {
  formShopping: FormGroup;
  shopping: Shopping;
  purchaseOrder: Oc;
  isReserveStock: boolean = true;
  isGenerateOc: boolean = true;
  isDetail: boolean = false;
  errors: Error;
  dni: string;
  channel: string;
  user: string;
  products: Product[];
  accounts: Account[];
  messages: String[];
  emailMessage: Message;

  constructor(private productService: ProductoService,
    private orderService: OrderService,
    private stockService: StockService,
    private notifierService: NotifierService,
    private deliveryService: DeliveryService,
    private fb: FormBuilder,
    public snackBar: MatSnackBar, private router: Router) {
  }

  ngOnInit() {
    this.messages = [];
    const token = localStorage.getItem('token');
    const tokenPayload = jwtDecode(token);
    this.dni = tokenPayload.dni;
    this.channel = tokenPayload.channel;
    this.user = tokenPayload.account;


    this.formShopping = this.fb.group({
      'product': ['', Validators.required],
      'quantity': ['', Validators.required],
      'document': [this.dni, Validators.required],
      'account': ['', Validators.required],
      'email': ['', Validators.required],
      'address': ['', Validators.required],
      'otp': [''],
    });

    this.buildProductsAndAccounts();
  }

  buildProductsAndAccounts(): void {
    this.products = [
      { id: "PROD00001", description: 'INDEX POLO CARIN PARA MUJER' },
      { id: "PROD00002", description: 'MARQUIS ZAPATILLAS' },
      { id: "PROD00003", description: 'LENOVO LAPTOP' }
    ];

    this.accounts = [
      { id: "000000-00000000-C", description: 'Ripley Clásica' },
      { id: "000000-00000000-S", description: 'Ripley Silver Mastercard' }
    ];
  }

  verifyStock(): void {
    let valid = this.formShopping.valid;
    if (valid) {
      this.productService.getVerifyStockProducto(this.formShopping.value.product.id,
        this.formShopping.value.quantity)
        .subscribe(response => {
          this.shopping = response.body;
          localStorage.setItem("activity", response.headers.get('X-Custom-Header'))
          this.messages.push("Se tiene (" + this.shopping.cantDisponible + ") unidades disponibles del producto " + this.shopping.product.name);
        },
          (err: HttpErrorResponse) => {
            if (401 == err.status) {
              this.validateExpiration();
            }
            this.isReserveStock = true;
            this.openSnackBar("Completar los campos requeridos", "Ok");

          });
      this.isReserveStock = false;
    } else {
      this.openSnackBar("Ingrese los datos solicitados", "X");
    }
  }

  reserveStock(): void {
    this.shopping.email = this.formShopping.value.email;
    this.shopping.requestQuantity = this.formShopping.value.quantity;
    this.productService.postReserveStockProducto(this.shopping)
      .subscribe(response => {
        this.shopping = response.body;
        localStorage.setItem("activity", response.headers.get('X-Custom-Header'))

        this.isGenerateOc = false;
        this.messages.push("Se ha reservado el producto, código de reserva: " + this.shopping.reservationCode);
        this.locateStock();
      },
        (err: HttpErrorResponse) => {
          if (401 == err.status) {
            this.validateExpiration();
          }
          this.openSnackBar(err.error, "X");
        });
  }

  locateStock(): void {
    this.stockService.locateStock(this.shopping)
      .subscribe(response => {
        this.shopping.local = response.body.local;
        localStorage.setItem("activity", response.headers.get('X-Custom-Header'))

        this.messages.push("Disponible en la tienda (" + this.shopping.local.name + ") ubicado en: " + this.shopping.local.address);
      },
        (err: HttpErrorResponse) => {
          if (401 == err.status) {
            this.validateExpiration();
          }
          this.openSnackBar(err.error, "X"), this.isDetail = false;
        });
  }

  validateOtp(): void {
    this.orderService.validateOTP(this.shopping, this.formShopping.value.otp)
      .subscribe(response => {
        this.messages.push("La validación de OTP es correcta");
        localStorage.setItem("activity", response.headers.get('X-Custom-Header'))
        this.generateOC();
      },
        (err: HttpErrorResponse) => {
          if (401 == err.status) {
            this.validateExpiration();
          }
          this.openSnackBar(err.error, "X"), this.isDetail = false;
        });
  }

  generateOC(): void {
    this.shopping.account = this.formShopping.value.account.id;
    this.orderService.generateOC(this.shopping, this.formShopping.value.otp, this.formShopping.value.address)
      .subscribe(response => {
        this.shopping = response.body;
        localStorage.setItem("activity", response.headers.get('X-Custom-Header'))

        this.messages.push("Se generó la orden de compra: " + this.shopping.code);
        this.decreaseStock();
      },
        (err: HttpErrorResponse) => {
          if (401 == err.status) {
            this.validateExpiration();
          }
          this.openSnackBar(err.error, "X"), this.isDetail = false;
        });
  }

  decreaseStock(): void {
    this.shopping.requestQuantity = this.formShopping.value.quantity;
    this.stockService.decreaseStock(this.shopping)
      .subscribe(response => {
        localStorage.setItem("activity", response.headers.get('X-Custom-Header'))

        this.messages.push("Se disminuyó el stock del producto: cant. comprada (" + response.body.cantComprada + "), cant. disponible (" + response.body.cantDisponible + ")");
        this.sendEmail();
      },
        (err: HttpErrorResponse) => {
          if (401 == err.status) {
            this.validateExpiration();
          }
          this.openSnackBar(err.error, "X"), this.isDetail = false;
        });
  }

  sendEmail(): void {
    this.emailMessage = {
      to: this.formShopping.value.email,
      text: "Gracias por tu compra<br/>" +
        "<br/>Reserva:" + this.shopping.reservationCode +
        "<br/>Orden de Compra: " + this.shopping.code +
        "<br/>Cuenta: " + this.shopping.account +
        "<br/><br/>Documento Identididad:" + this.dni +
        "<br/>Producto: " + this.shopping.product.name +
        "<br/>Cantidad:" + this.shopping.requestQuantity +
        "<br/>Precio Unitario: S/." + this.shopping.product.amount +
        "<br><br> Total: S/." + this.shopping.amount
    };

    this.notifierService.email(this.emailMessage)
      .subscribe(data => {
        this.messages.push("Se envió el correo con la orden de compra (" + this.emailMessage.to + "):<br/>*" + this.emailMessage.text + "*");
        this.deliveryAddress();
      },
        (err: HttpErrorResponse) => {
          if (401 == err.status) {
            this.validateExpiration();
          }
          this.openSnackBar(err.error, "X"), this.isDetail = false;
        });
  }

  deliveryAddress(): void {
    this.deliveryService.deliveryAddress(this.shopping, this.formShopping.value.address)
      .subscribe(response => {
        this.purchaseOrder = response.body;
        localStorage.setItem("activity", response.headers.get('X-Custom-Header'))

        this.messages.push("El producto será enviado a la dirección: " + this.formShopping.value.address);
        this.isDetail = true;
      },
        (err: HttpErrorResponse) => {
          if (401 == err.status) {
            this.validateExpiration();
          }
          this.openSnackBar(err.error, "X"), this.isDetail = false;
        });

  }

  private validateExpiration() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}

